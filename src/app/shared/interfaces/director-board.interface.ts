export interface DirectorBoardEnumInterface {
  enumCode: string;
  enumLabel: string;
}
