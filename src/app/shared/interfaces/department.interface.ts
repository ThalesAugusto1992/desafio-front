import {Ufs} from './ufs.interface';

export interface Department {
  id: number;
  code: string;
  name: string;
  local: string;
  city: string;
  uf: Ufs;
  directorBoard: string;
}
