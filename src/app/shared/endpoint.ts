import {environment} from '../../environments/environment';

export const API_URL = {
  UF: {
    GET_ALL: `${environment.baseUrl}ufs`
  },
  DEPARTMENT: {
    GET_DIRECTORS_BOARD: `${environment.baseUrl}departments/director-boards`,
    DEPARTMENT_OPERATIONS: `${environment.baseUrl}departments/`,
  }
};
