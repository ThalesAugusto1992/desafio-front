import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Ufs} from '../interfaces/ufs.interface';
import {API_URL} from '../endpoint';

@Injectable({
  providedIn: 'root'
})
export class UfService {

  constructor(private http: HttpClient) {
  }

  getUfs(): Observable<any> {
    return this.http.get(API_URL.UF.GET_ALL);
  }
}
