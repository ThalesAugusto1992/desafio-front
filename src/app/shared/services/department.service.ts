import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {API_URL} from '../endpoint';
import {Department} from '../interfaces/department.interface';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private http: HttpClient) {
  }

  getDepartmentsWithParams(code ?: string, name ?: string, local ?: string, city?: string, page ?: number, size ?: number): Observable<any> {
    const params = new HttpParams()
      .set('code', code || '')
      .set('name', name || '')
      .set('local', local || '')
      .set('city', city || '')
      .set('page', String(page || 0))
      .set('size', String(size || 10));

    return this.http.get(API_URL.DEPARTMENT.DEPARTMENT_OPERATIONS, {params});
  }

  deleteDepartment(id: number): Observable<any> {
    return this.http.delete(API_URL.DEPARTMENT.DEPARTMENT_OPERATIONS + id);
  }

  getDirectorsBoards(): Observable<any> {
    return this.http.get(API_URL.DEPARTMENT.GET_DIRECTORS_BOARD);
  }

  saveDepartment(entity: Department): Observable<any> {
    return this.http.post(API_URL.DEPARTMENT.DEPARTMENT_OPERATIONS, entity);
  }

  updateDepartment(id: number, entity: Department): Observable<any> {
    return this.http.put(API_URL.DEPARTMENT.DEPARTMENT_OPERATIONS + id, entity);
  }

  getDepartment(id: number): Observable<any> {
    return this.http.get<any>(API_URL.DEPARTMENT.DEPARTMENT_OPERATIONS + id);
  }


}
