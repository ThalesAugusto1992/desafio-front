import {Component, OnInit} from '@angular/core';
import {UfService} from '../../shared/services/uf.service';
import {Ufs} from '../../shared/interfaces/ufs.interface';
import {DepartmentService} from '../../shared/services/department.service';
import {DirectorBoardEnumInterface} from '../../shared/interfaces/director-board.interface';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Department} from '../../shared/interfaces/department.interface';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  listUfs: Array<Ufs> = new Array<Ufs>();

  listDirectorBoards: Array<DirectorBoardEnumInterface> = new Array<DirectorBoardEnumInterface>();

  form: FormGroup;

  submitted = false;

  constructor(private routerParam: ActivatedRoute,
              private ufService: UfService,
              private departmentService: DepartmentService,
              private formBuilder: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [null],
      code: ['', [Validators.required]],
      name: ['', [Validators.required]],
      local: ['', [Validators.required]],
      city: ['', [Validators.required]],
      ufs: ['', [Validators.required]],
      directorBoard: ['', [Validators.required]]
    });

    this.getUfs();
    this.getDirectorsBoards();

    if (this.routerParam.params) {
      this.routerParam.params.subscribe(params => {
        if (params.id) {
          this.getDepartment(params.id);
        }
      });
    }
  }

  submitForm(): void {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    const entity = this.fillEntity();

    if (this.getId() != null) {
      this.departmentService.updateDepartment(this.getId().value, entity)
        .subscribe(() => this.router.navigate(['/home']));
    } else {
      this.departmentService.saveDepartment(entity)
        .subscribe(() => this.router.navigate(['/home']));
    }

  }

  fillEntity(): Department {
    return {
      id: this.getId().value,
      code: this.getCode().value,
      name: this.getName().value,
      local: this.getLocal().value,
      city: this.getCity().value,
      uf: this.listUfs.find(element => element.id == this.getUf().value),
      directorBoard: this.getDirectorBoard().value
    };
  }

  getUfs(): void {
    this.ufService.getUfs()
      .subscribe(res => this.listUfs = res.data);
  }

  getDirectorsBoards(): void {
    this.departmentService.getDirectorsBoards()
      .subscribe(res => this.listDirectorBoards = res.data);
  }

  getDepartment(id: number): void {
    this.departmentService.getDepartment(id)
      .subscribe(res => {
        const data = res.data;
        this.getId().setValue(data.id);
        this.getCode().setValue(data.code);
        this.getName().setValue(data.name);
        this.getLocal().setValue(data.local);
        this.getCity().setValue(data.city);
        this.getUf().setValue(data.uf.id);
        this.getDirectorBoard().setValue(data.directorBoard);
      });
  }

  isInvalid(control: AbstractControl): string {
    if (control.errors && !this.submitted) {
      return control.value !== '' ? 'is-invalid' : '';
    }

    if (control.errors && this.submitted) {
      return 'is-invalid';
    }

    return 'is-valid';
  }

  getId(): AbstractControl {
    return this.form.get('id');
  }

  getCode(): AbstractControl {
    return this.form.get('code');
  }

  getName(): AbstractControl {
    return this.form.get('name');
  }

  getLocal(): AbstractControl {
    return this.form.get('local');
  }

  getCity(): AbstractControl {
    return this.form.get('city');
  }

  getUf(): AbstractControl {
    return this.form.get('ufs');
  }

  getDirectorBoard(): AbstractControl {
    return this.form.get('directorBoard');
  }
}
