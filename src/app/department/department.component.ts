import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';
import {DepartmentService} from '../shared/services/department.service';
import {Department} from '../shared/interfaces/department.interface';
import {Alert} from '../shared/interfaces/alert.interface';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  form: FormGroup;

  page = 0;
  size = 10;

  totalOfElements = 0;
  listOfDepartments: Array<Department> = new Array<Department>();

  alerts: Array<Alert> = new Array<Alert>();

  constructor(private formBuilder: FormBuilder,
              private departmentService: DepartmentService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      code: '',
      name: '',
      local: '',
      city: ''
    });

    this.getListOfDepartments();
  }


  getListOfDepartments(): void {
    this.departmentService.getDepartmentsWithParams(this.getCode().value, this.getName().value, this.getLocal().value, this.getCity().value, this.page, this.size)
      .subscribe(res => {
        this.totalOfElements = res.data.totalElements;
        this.listOfDepartments = res.data.content;
      });
  }

  updateItem(update: boolean): void {
    if (update) {
      this.alerts.push({
        type: 'success',
        timeout: 5000,
        msg: 'Departamento excluido com sucesso'
      });
      this.getListOfDepartments();
    } else {
      this.alerts.push({
        type: 'danger',
        timeout: 5000,
        msg: 'Erro ao deletar Departamento'
      });
    }
  }

  onCloseAlert(alert: Alert): void {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  changeTableSize($event): void {
    this.size = $event.target.value;
    this.getListOfDepartments();
  }

  changeTablePage($event): void {
    this.page = $event.page - 1;
    this.getListOfDepartments();
  }

  getCode(): AbstractControl {
    return this.form.get('code');
  }

  getName(): AbstractControl {
    return this.form.get('name');
  }

  getLocal(): AbstractControl {
    return this.form.get('local');
  }

  getCity(): AbstractControl {
    return this.form.get('city');
  }
}
