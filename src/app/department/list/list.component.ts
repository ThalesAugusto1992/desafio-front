import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {Department} from '../../shared/interfaces/department.interface';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {DepartmentService} from '../../shared/services/department.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {


  @Input()
  listOfDepartments: Array<Department> = new Array<Department>();

  @Output()
  updateList = new EventEmitter();

  modalRef: BsModalRef;
  focalId: number;


  constructor(private modalService: BsModalService,
              private departmentService: DepartmentService) {
  }

  ngOnInit(): void {
  }

  deleteDepartment(template: TemplateRef<any>, id: number): void {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    this.focalId = id;
  }

  confirmDeletion(confirm: boolean): void {
    if (confirm) {
      this.departmentService.deleteDepartment(this.focalId).subscribe(() => {
        },
        error => {

        },
        () => {
          this.updateList.emit(true);
          this.modalRef.hide();
        });
    } else {
      this.modalRef.hide();
    }
  }


}
