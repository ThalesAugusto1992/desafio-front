import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRouterModule} from './router/app-router.module';
import {DepartmentComponent} from './department/department.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ListComponent} from './department/list/list.component';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {HttpClientModule} from '@angular/common/http';
import {ModalModule} from 'ngx-bootstrap/modal';
import {AlertModule} from 'ngx-bootstrap/alert';
import { FormComponent } from './department/form/form.component';

@NgModule({
  declarations: [
    AppComponent,
    DepartmentComponent,
    ListComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    HttpClientModule,
    ModalModule.forRoot(),
    AlertModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
